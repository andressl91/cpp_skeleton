#include "doctest/doctest.h"

int foo(int x) {
    return x;
}
TEST_SUITE("foo"){
    TEST_CASE("bar") {
    int a = 1;
    CHECK(foo(1) == 1);
    SUBCASE("foobar") {
        CHECK(a == 1);
        }
    }
}
