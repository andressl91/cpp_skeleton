PROJECT_WORKSPACE=${PROJECT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../)}
PROJECT_DEPENDENCIES=$PROJECT_WORKSPACE/dependencies

cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER"
build_doctest="{{ cookiecutter.doctest }}"
build_criterion="{{ cookiecutter.criterion }}"

cd ${PROJECT_DEPENDENCIES}
if [ ${build_doctest} == "Y" ]; then
    if [ ! -d doctest ]; then
        echo Building doctest
        git clone https://github.com/onqtam/doctest
        pushd doctest
        cmake . -DCMAKE_INSTALL_PREFIX="${PROJECT_DEPENDENCIES}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
        make install -j4
        popd
    fi;
    echo Done doctest
fi;

if [ ${build_criterion} == "Y" ]; then
    if [ ! -d criterion ]; then
        echo Building criterion
        git clone https://github.com/p-ranav/criterion.git
        pushd criterion
        cmake . -DCMAKE_INSTALL_PREFIX="${PROJECT_DEPENDENCIES}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
        make install -j4
        popd
    fi;
    echo Done Criterion
fi;
